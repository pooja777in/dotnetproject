﻿namespace BurgerApp.Models
{
    public class Burger
    {
        public int Id { get; set; }
        public string Burger_Name { get; set; }
        public string Burger_Location { get; set; } 
        public int Quantity { get; set; }
        public float Price { get; set; }
        }
    }

